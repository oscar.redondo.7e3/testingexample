package cat.itb.m07.testting;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;


import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.pressBack;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isRoot;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    private static String USER_TO_BE_TYPED = "oscar";
    private static String PASS_TO_BE_TYPED = "oscar";

    @Rule
    public ActivityScenarioRule<MainActivity>activityActivityScenarioRule =new ActivityScenarioRule<MainActivity>(MainActivity.class);

    @Test
    public void elementsDisplayedCorrectlyOnMainActivity() {

        onView(withId(R.id.MainActivityTextView)).check(matches(isDisplayed()));
        onView(withId(R.id.buton)).check(matches(isDisplayed()));

    }


    @Test
    public void checkTitleTextIsCorrect(){
        onView(withId(R.id.MainActivityTextView)).check(matches(withText(R.string.main_activity_title)));
        onView(withId(R.id.buton)).check(matches(withText(R.string.next)));
    }

    @Test
    public void buttonTextIsCorrectOnClick(){
        onView(withId(R.id.buton)).check(matches(isClickable()));
        onView(withId(R.id.buton)).perform(click()).check(matches(withText("back")));

    }

    @Test
    public void login_form_behaviour(){
        onView(withId(R.id.editText_username)).perform(replaceText(USER_TO_BE_TYPED));
        onView(withId(R.id.editText_password)).perform(replaceText(PASS_TO_BE_TYPED));
        onView(withId(R.id.buton)).perform(click()).check(matches(withText("Logged")));
    }

    @Test
    public void start_secondActivity_onclick_button(){
        onView(withId(R.id.buton)).perform(click());
        onView(withId(R.id.secondLayout)).check(matches(isDisplayed()));

    }



    @Test
    public void back_Mainactivirt_onClicn_back(){
        onView(withId(R.id.buton)).perform(click());
        onView(withId(R.id.secondLayout)).check(matches(isDisplayed()));
        onView(withId(R.id.button_secondActivity)).perform(click());
        onView(withId(R.id.mainActivity)).check(matches(isDisplayed()));
        onView(withId(R.id.buton)).perform(click());
        onView(withId(R.id.secondLayout)).check(matches(isDisplayed()));
        onView(withId(R.id.button_secondActivity)).perform(click());
        onView(withId(R.id.mainActivity)).check(matches(isDisplayed()));
        onView(isRoot()).perform(pressBack());
    }

    @Test
    public void full_funcionality(){
        onView(withId(R.id.editText_username)).perform(replaceText(USER_TO_BE_TYPED));
        onView(withId(R.id.editText_password)).perform(replaceText(PASS_TO_BE_TYPED));
        onView(withId(R.id.buton)).perform(click());
        onView(withId(R.id.secondLayout)).check(matches(isDisplayed()));
        onView(withId(R.id.TextView_welcome)).check(matches(withText("Welcome back "+USER_TO_BE_TYPED)));
        onView(withText(R.id.button_secondActivity)).perform(click());
        onView(withId(R.id.mainActivity)).check(matches(isDisplayed()));
        onView(withId(R.id.editText_username)).check(matches(withText("")));
        onView(withId(R.id.editText_password)).check(matches(withText("")));
    }

}
