package cat.itb.m07.testting;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    private Button b;
    private TextView welcome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        String name = getIntent().getStringExtra("name");

        welcome = findViewById(R.id.TextView_welcome);
        welcome.setText("Welcome back "+name);

        b= findViewById(R.id.button_secondActivity);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(SecondActivity.this,MainActivity.class);
                startActivity(in);

            }
        });

    }
}