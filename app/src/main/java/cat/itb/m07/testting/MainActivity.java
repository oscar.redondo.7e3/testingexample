package cat.itb.m07.testting;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    TextView title;
    Button b;
    EditText editTextUsername, editTextPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        editTextPassword=findViewById(R.id.editText_password);
        editTextUsername = findViewById(R.id.editText_username);
        title = findViewById(R.id.MainActivityTextView);
        b = findViewById(R.id.buton);
        final Intent i = new Intent(MainActivity.this,SecondActivity.class);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editTextPassword.getText().toString().equals("")){
                    i.putExtra("name",editTextUsername.getText().toString());
                    b.setText("back");
                    startActivity(i);
                }else {
                    b.setText("Logged");
                    i.putExtra("name",editTextUsername.getText().toString());
                    startActivity(i);
                }

            }
        });

    }
}